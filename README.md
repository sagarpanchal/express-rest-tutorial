# Setup Instructions

### Clone this repo in current directory

**Create and switch to a new empty directory and run following command**

```
https://gitlab.com/sagarpanchal/express-rest-tutorial.git .
```

**Generate RSA keys for JWT**

```
rm -rf .keys && mkdir -p .keys && cd .keys && ../scripts/gen-keys.sh && cd ..
```

**Install all dependencies**

```
npm i
```

**Run the project**

```
npm start     # runs `npm run dev`
npm run dev   # runs with NODE_ENV=development
npm run prod  # runs with NODE_ENV=production
```

<small>see `scripts` in `package.json`</small>

### OPTIONAL

**ESLint plugins for vscode**

```
npm i -g eslint eslint-config-node eslint-config-esnext
```

```
code --install-extension dbaeumer.vscode-eslint
```
