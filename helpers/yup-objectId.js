const mixed = require('yup/lib/mixed')
const ObjectId = require('bson/lib/objectid')

class ObjectIdSchema extends mixed {
  constructor() {
    super({ type: 'objectId' })
  }

  _typeCheck(value) {
    return ObjectId.isValid(value)
  }
}

module.exports = () => new ObjectIdSchema()
