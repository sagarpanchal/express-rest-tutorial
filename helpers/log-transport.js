const Transport = require('winston-transport')
const debug = require('debug')('app:helpers:transport')
const { sync: mkdirp } = require('mkdirp')
const path = require('path')
const fs = require('fs')

module.exports = class CustomTransport extends Transport {
  constructor(opts) {
    super(opts)
    this.filename = opts.filename
    this.setup()
  }

  initialize() {
    try {
      // Recursively create directories from path
      mkdirp(path.parse(this.filename).dir)
      // Write empty file
      fs.writeFileSync(this.filename, [], 'utf8')
    } catch (error) {
      debug(error)
    }
  }

  setup() {
    // This checks if the file exists
    if (fs.existsSync(this.filename)) {
      // The content of the file is checked to know if it is necessary to adapt the array
      try {
        const data = fs.readFileSync(this.filename, 'utf8')
        // If the content of the file is not an array, it is set
        const content = JSON.parse(data)
        if (!Array.isArray(content)) {
          this.initialize()
        }
      } catch (error) {
        this.initialize()
      }
    }
    // Otherwise create the file with the desired format
    else {
      this.initialize()
    }
  }

  readLog() {
    let data = null
    try {
      data = fs.readFileSync(this.filename, 'utf8')
    } catch (error) {
      debug(error)
    }
    return data
  }

  writeLog(info) {
    const data = this.readLog()
    let arr = []
    if (data) {
      arr = JSON.parse(data)
    }
    // Add data
    arr.push(info)
    // Convert data back to json
    const json =
      process.env.NODE_ENV === 'development'
        ? JSON.stringify(arr, ' ', 2)
        : JSON.stringify(arr)
    try {
      // Writing the array again
      fs.writeFileSync(this.filename, json, 'utf8')
    } catch (error) {
      debug(error)
    }
  }

  log(info, callback) {
    setImmediate(() => {
      this.emit('logged', info)
    })
    // Perform the writing
    this.writeLog(info)

    callback()
  }
}
