const nodemailer = require('nodemailer')
const config = require('config')

var transporter = nodemailer.createTransport(config.get('mailer'))

module.exports = async (to, subject, html) => {
  const from = `Vidly <${config.get('mail-from')}>`
  to = `User <${to}>`
  return await transporter.sendMail({ from, to, subject, html })
}
