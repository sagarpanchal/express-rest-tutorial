module.exports = {
  getErrors: (schema, object) => {
    if (module.exports.hasErrors(schema, object)) {
      let message = { name: 'Error', error: 'something went wrong' }
      try {
        schema.validateSync(object)
      } catch (e) {
        return { name: e.name, error: e.errors[0] }
      }
      return message
    }
    return false
  },
  hasErrors: (schema, object) => {
    return !schema.isValidSync(object)
  }
}
