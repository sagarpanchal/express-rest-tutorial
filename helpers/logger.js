const path = require('path')
const config = require('config')
const winston = require('winston')
const customTransport = require('./log-transport')
require('winston-mongodb')

const env = process.env.NODE_ENV
const year = new Date().getFullYear().toString()
const month = ('0' + (new Date().getMonth() + 1)).slice(-2).toString()
const day = ('0' + new Date().getDate()).slice(-2).toString()
const name = `${year}-${month}-${day}.log.json`
const filename = path.join(__dirname, '..', '.logs', env, year, month, name)
const format = winston.format.timestamp()
const transports = []

config.get('logger') === 'json' &&
  transports.push(new customTransport({ filename }))

config.get('logger') === 'database' &&
  transports.push(
    new winston.transports.MongoDB({ db: config.get('db') || process.env.DB })
  )

transports.length === 0 &&
  transports.push(new winston.transports.File({ filename }))

module.exports = winston.createLogger({ format, transports })
