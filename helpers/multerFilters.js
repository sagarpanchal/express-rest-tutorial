exports.imageFilter = (req, file, cb) => {
  const mimeTypes = ['image/jpg', 'image/jpeg', 'image/png']
  if (!mimeTypes.includes(file.mimetype)) {
    req.mimeCheckFailed = true
    return cb(null, false)
  }
  req.mimeCheckFailed = false
  req.sizeCheckFailed = false
  cb(null, true)
}
