const fs = require('fs')
const path = require('path')
const jwt = require('jsonwebtoken')
const debug = require('debug')('app:helper:jwt')

const getRSA = (type = 'cert') => {
  const file = type === 'private' ? 'jwt_rsa.key' : 'jwt_rsa.key.pub'
  return fs.readFileSync(path.join(__dirname, '..', '.keys', file)).toString()
}

/**
 * Generate and return JWT with specified object
 * @param {Object} object
 */
exports.genJWT = object => {
  const options = { algorithm: 'RS256', expiresIn: '1h' }
  try {
    return jwt.sign(object, getRSA('private'), options)
  } catch (e) {
    debug(e.message)
    return false
  }
}

/**
 * Verify JWT
 * @param {Object} token Json Web Token
 */
exports.verifyJWT = token => {
  try {
    return jwt.verify(token, getRSA('cert'), { algorithm: 'RS256' })
  } catch (e) {
    debug(e.message)
    return false
  }
}
