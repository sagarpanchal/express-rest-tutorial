const path = require('path')
const pug = require('pug')
const sendMail = require('./sendMail')

exports.sendVerificationEmail = async (name, email, verificationKey) => {
  const subject = '[Vidly] Verify your email'
  const reciever = name.split(' ').slice(0, 1)[0]
  const body = { subject, name: reciever, verificationKey }
  var emailTemplate = path.join(
    __dirname,
    '..',
    'views',
    'emails',
    'verify-email.pug'
  )
  const html = pug.renderFile(emailTemplate, body)
  return await sendMail(email, subject, html)
}
