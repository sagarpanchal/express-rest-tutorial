'use strict'
const express = require('express')
const config = require('config')
const mongoose = require('mongoose')
const debug = require('debug')('app:startup')
const logger = require('./helpers/logger.js')
require('express-async-errors')

/**
 * Connect MongoDB using mongoose
 */
const DB = config.get('db') || process.env.DB
mongoose
  .connect(DB, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true
  })
  .then(() => debug(`\x1b[32mConnected to \x1b[0m${DB}`))
  .catch(err => {
    debug(
      '\x1b[31m--- Failed to connect to MongoDB. Check logs for details ---\x1b[0m'
    )
    logger.error('', err)
    process.exit(1)
  })

/**
 * Create an Express application
 */
const app = express()

try {
  /**
   * Template engine
   */
  app.set('view engine', 'pug').set('views', './views')

  /**
   * Middleware Before Routes
   */
  require('./middleware/_middlewareBefore')(app, express)

  /**
   * Routes
   */
  require('./routes')(app)

  /**
   * Middleware After Routes
   */
  require('./middleware/_middlewareAfter')(app)
} catch (e) {
  logger.error('', e)
  debug('\x1b[31m--- Something went wrong. Check logs for details ---\x1b[0m')
  process.exit(1)
}

/**
 * Listen to requests
 */
const PORT = config.get('port') || process.env.PORT
const server = app.listen(PORT || 3000)
debug('\x1b[2J')
debug(`\x1b[32mListening on port - \x1b[36m${PORT}\x1b[0m`)

/**
 * Handle server shutdown
 */
server.on('close', async () => {
  debug(`\x1b[32mServer closed on port - \x1b[36m${PORT}\x1b[0m`)
  await mongoose.connection.close(false, async () => {
    await debug(`\x1b[32mMongoDb connection closed\x1b[0m`)
  })
})

/**
 * Shutdown server and log the reason for that
 * @param {STRING} signal
 * @param {Exception} err
 */
const shutdown = async (signal = false, err = false) => {
  if (typeof err !== 'number' && err) {
    logger.error('', err)
    debug('\x1b[31m--- Something went wrong. Check logs for details ---\x1b[0m')
  }
  if (signal) {
    logger.info(signal)
    debug(`\x1b[32mReceived ${signal} on port - \x1b[36m${PORT}\x1b[0m`)
  }
  await server.close()
}

/**
 * Handle terminating events
 */
process.on('SIGINT', shutdown)
process.on('SIGQUIT', shutdown)
process.on('SIGTERM', shutdown)
process.on('uncaughtException', err => shutdown(false, err))
process.on('unhandledRejection', err => shutdown(false, err))
