const express = require('express')
const bcrypt = require('bcrypt')
const yup = require('yup')
const debug = require('debug')('app:route:auth')
const uuid = require('uuid/v4')
const schemaValidator = require('../helpers/schemaValidator')
const { sendVerificationEmail } = require('../helpers/email')
const { email, password } = require('../helpers/validationSchema')
const errors = require('../helpers/errorMessages')
const { genJWT } = require('../helpers/jwt')
const { User } = require('../models/user')

const router = express.Router()
const { accessDenied, sendError, wentWrong } = errors

const schema = yup.object().shape({
  email: email(),
  password: password()
})

const getErrors = object => schemaValidator.getErrors(schema, object)

router.post('/', async (req, res) => {
  const error = getErrors(req.body)
  if (error) return res.status(400).json(error)

  let user = await User.findOne({ email: req.body.email })
  if (!user) return sendError(res, 'Invalid email or password')
  if (!user.verified)
    return accessDenied(res, 'Please verify your email before logging in')

  const match = await bcrypt.compare(req.body.password, user.password)
  if (!match) return sendError(res, 'Invalid email or password')

  const token = genJWT({ _id: user._id })
  return res.json({ token })
})

router.get('/verify/:uuid', async (req, res) => {
  const user = await User.findOneAndUpdate(
    { verificationKey: req.params.uuid },
    { verificationKey: uuid(), verified: true },
    { new: true }
  )

  debug(user)
  if (!user) return sendError(res, 'Verification link expired')

  res.json({ message: 'Email verified' })
})

router.get('/resend-verification/:email', async (req, res) => {
  const user = await User.findOne({ email: req.params.email })
  if (!user) return sendError(res, 'Email is not registered')
  if (user.verified) return sendError(res, 'Email is already verified')

  const { name, email, verificationKey } = user
  const status = await sendVerificationEmail(name, email, verificationKey)
  if (!status) return wentWrong(res)

  res.json({ registered: true, verified: false })
})

module.exports = router
