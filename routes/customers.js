const express = require('express')
const { notFound } = require('../helpers/errorMessages')
const { Customer, getErrors } = require('../models/customer')

const router = express.Router()

/**
 * Get all customers
 */
router.get('/', async (req, res) => {
  return res.json(await Customer.find().sort('name'))
})

/**
 * Get a customer by id
 */
router.get('/:_id', async (req, res) => {
  const { _id } = req.params

  const customer = await Customer.find({ _id }).sort('name')
  !customer ? notFound(res, 'customer') : res.json(customer)
})

/**
 * Add new customers
 */
router.post('/', async (req, res) => {
  let error = getErrors(req.body)
  if (error) return res.status(404).json(error)

  const customer = await new Customer(req.body).save()
  return res.json(customer)
})

/**
 * Update customer details
 */
router.put('/:_id', async (req, res) => {
  const error = getErrors(req.body)
  if (error) return res.status(404).json(error)

  const { _id } = req.params
  const { name, isGold, phone } = req.body

  const customer = await Customer.findOneAndUpdate(
    { _id },
    { name, isGold, phone },
    { new: true }
  )
  if (!customer) return notFound(res, 'customer')
  return res.json(customer)
})

/**
 * Delete a customer
 */
router.delete('/:_id', async (req, res) => {
  const { _id } = req.params

  const customer = await Customer.findByIdAndRemove(_id)
  if (!customer) return notFound(res, 'customer')
  return res.json(customer)
})

module.exports = router
