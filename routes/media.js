const express = require('express')
const debug = require('debug')('app:route:media')
const multer = require('multer')
const path = require('path')
const fs = require('fs')
const fileType = require('file-type')
const errors = require('../helpers/errorMessages')
const { imageFilter } = require('../helpers/multerFilters')
const { notFound, unsupportedMediaType } = errors

const router = express.Router()

const images = multer({
  dest: '.uploads/images/',
  fileFilter: imageFilter,
  limits: { fileSize: 5368709120 }
})

router.get('/image/:id', async (req, res) => {
  const file = path.join(__dirname, '..', '.uploads', 'images', req.params.id)
  if (fs.existsSync(file)) {
    const data = fs.readFileSync(file)
    const { mime } = fileType(data)
    return res.type(mime).send(data)
  }
  return notFound(res, 'File')
})

router
  .use(images.single('image'), (err, req, res, next) => {
    if (err instanceof multer.MulterError)
      return errors.sendError(res, err.message)
    next()
  })
  .post('/image', async (req, res) => {
    return req.mimeCheckFailed
      ? unsupportedMediaType(res, 'Invalid image format')
      : res.json({ file: req.file.filename })
  })

module.exports = router
