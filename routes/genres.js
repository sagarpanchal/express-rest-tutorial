const express = require('express')
const ObjectId = require('bson/lib/objectid')
const { notFound, sendError } = require('../helpers/errorMessages')
const { Genre, getErrors } = require('../models/Genre')

const router = express.Router()

router.get('/', async (req, res) => {
  return res.json(await Genre.find().sort('name'))
})

router.get('/:id', async (req, res) => {
  const { id } = req.params
  if (!ObjectId.isValid(id)) return sendError(res, 'Invalid object id')

  const genre = await Genre.findById(id).sort('name')
  return typeof genre === 'object' ? res.json(genre) : notFound(res, 'Genre')
})

router.post('/', async (req, res) => {
  const error = getErrors(req.body)
  if (error) return res.status(404).json(error)

  const { name } = req.body

  let genre = new Genre({ name })
  genre = await genre.save()
  return res.json(genre)
})

router.put('/:id', async (req, res) => {
  const error = getErrors(req.body)
  if (error) return res.status(404).json(error)

  const { id } = req.params
  const { name } = req.body

  const genre = await Genre.findByIdAndUpdate(id, { name }, { new: true })
  if (!genre) return notFound(res, 'Genre')
  return res.json(genre)
})

router.delete('/:id', async (req, res) => {
  const { id } = req.params

  const genre = await Genre.findByIdAndRemove(id)
  if (!genre) return notFound(res, 'Genre')
  return res.json(genre)
})

module.exports = router
