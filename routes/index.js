const routeHome = require('../routes/home')
const routeAuth = require('../routes/auth')
const routeUsers = require('../routes/users')
const routeGenres = require('../routes/genres')
const routeMovies = require('../routes/movies')
const routeCustomers = require('../routes/customers')
const routeRentals = require('../routes/rentals')
const routeMedia = require('../routes/media')

module.exports = app => {
  app
    .use('/', routeHome)
    .use('/media/', routeMedia)
    .use('/api/auth/', routeAuth)
    .use('/api/users/', routeUsers)
    .use('/api/genres/', routeGenres)
    .use('/api/movies/', routeMovies)
    .use('/api/customers/', routeCustomers)
    .use('/api/rentals/', routeRentals)
}
