const express = require('express')
const bcrypt = require('bcrypt')
const debug = require('debug')('app:route:users')
const uuid = require('uuid/v4')
const { sendVerificationEmail } = require('../helpers/email')
const { sendError, wentWrong } = require('../helpers/errorMessages')
const { User, getErrors } = require('../models/user')

const router = express.Router()

router.get('/me', async (req, res) => {
  const { _id } = req.user
  const user = await User.findById(_id).select(['name', 'email', '-_id'])
  return res.json(user)
})

router.post('/', async (req, res) => {
  const error = getErrors(req.body)
  if (error) return res.status(400).json(error)

  const { name, email, isAdmin } = req.body
  const findUser = await User.countDocuments({ email })
  debug(findUser)
  if (findUser) return sendError(res, 'User already exists.')

  const password = await bcrypt.hash(req.body.password, 10)
  const verificationKey = uuid().toString()

  const data = { name, email, password, verificationKey, isAdmin }
  const user = await new User(data).save()
  debug(user)

  const status = await sendVerificationEmail(name, email, verificationKey)
  if (!status) return wentWrong(res)

  return res.json({ registered: true, verified: false })
})

module.exports = router
