const express = require('express')
const Fawn = require('fawn')
const mongoose = require('mongoose')
const ObjectId = require('bson/lib/objectid')
const { notFound, sendError } = require('../helpers/errorMessages')
const { Rental, getErrors } = require('../models/rental')
const { Customer } = require('../models/customer')
const { Movie } = require('../models/movie')

const router = express.Router()

Fawn.init(mongoose)

router.get('/', async (req, res) => {
  return res.json(
    await Rental.find()
      .sort('-dateOut')
      .populate('customer', 'name')
      .populate('movie', 'title')
  )
})

router.get('/:id', async (req, res) => {
  const { id } = req.params
  if (!ObjectId.isValid(id)) return sendError(res, 'invalid object id')

  const rental = await Rental.findById(id).sort('-dateOut')
  return typeof rental === 'object' ? res.json(rental) : notFound(res, 'rental')
})

router.post('/', async (req, res) => {
  const error = getErrors(req.body)
  if (error) return res.status(400).json(error)

  const { customerId, movieId } = req.body

  const customer = await Customer.countDocuments({ _id: customerId })
  if (!customer) return notFound(res, 'customer')

  const movie = await Movie.findById(movieId)
  if (!movie) return notFound(res, 'movie')
  if (!movie.numberInStock) return sendError(res, 'movie out of stock')

  const { dailyRentalRate } = movie

  let rental = new Rental({
    customer: customerId,
    movie: movieId,
    dailyRentalRate
  })

  new Fawn.Task()
    .save('rentals', rental)
    .update('movies', { _id: movie._id }, { $inc: { numberInStock: -1 } })
    .run()

  return res.json(rental)
})

router.delete('/:id', async (req, res) => {
  const { id } = req.params
  if (!ObjectId.isValid(id)) return sendError(res, 'invalid object id')

  const rental = await Rental.findByIdAndRemove(id)
  return typeof rental === 'object' ? res.json(rental) : notFound(res, 'rental')
})

module.exports = router
