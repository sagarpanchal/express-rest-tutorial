const express = require('express')
const ObjectId = require('bson/lib/objectid')
const { notFound, sendError } = require('../helpers/errorMessages')
const { Genre } = require('../models/genre')
const { Movie, getErrors } = require('../models/movie')

const router = express.Router()

router.get('/', async (req, res) => {
  return res.json(
    await Movie.find()
      .sort('name')
      .populate('genre', 'name')
  )
})

router.get('/:_id', async (req, res) => {
  const { id } = req.params
  if (!ObjectId.isValid(id)) return sendError(res, 'invalid object id')

  const movie = await Movie.findById(id)
    .populate('genre', 'name')
    .sort('name')
  return typeof movie === 'object' ? res.json(movie) : notFound(res, 'movie')
})

router.post('/', async (req, res) => {
  let error = getErrors(req.body)
  if (error) return res.status(400).json(error)

  const { title, genreId, numberInStock, dailyRentalRate } = req.body

  let genre = await Genre.findById(genreId)
  if (!genre) return notFound(res, 'genre')

  const movie = await new Movie({
    title,
    genre: genreId,
    numberInStock,
    dailyRentalRate
  }).save()

  return res.json(movie)
})

router.put('/:_id', async (req, res) => {
  let error = getErrors(req.body)
  if (error) return res.status(400).json(error)

  const { _id } = req.params
  const { title, genreId, numberInStock, dailyRentalRate } = req.body

  const genre = await Genre.countDocuments({ _id: genreId })
  if (!genre) return notFound(res, 'genre')

  const movie = await Movie.findOneAndUpdate(
    { _id },
    { title, genre: genreId, numberInStock, dailyRentalRate },
    { new: true }
  )
  if (!movie) return notFound(res, 'movie')
  return res.json(movie)
})

router.delete('/:_id', async (req, res) => {
  const { _id } = req.params

  const movie = await Movie.findByIdAndRemove(_id)
  if (!movie) return notFound(res, 'movie')
  return res.json(movie)
})

module.exports = router
