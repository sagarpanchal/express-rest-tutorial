const Mongoose = require('mongoose')
const yup = require('yup')
const validationSchema = require('../helpers/validationSchema')
const schemaValidator = require('../helpers/schemaValidator')

const schema = yup.object().shape({
  title: validationSchema.requiredString('Title'),
  genreId: validationSchema.objectId('Genre Id'),
  numberInStock: validationSchema.positiveInt('Number in stock'),
  dailyRentalRate: validationSchema.positiveInt('Daily rental rate')
})

const hasErrors = object => schemaValidator.hasErrors(schema, object)
const getErrors = object => schemaValidator.getErrors(schema, object)

const numberSchema = {
  type: Number,
  required: true,
  min: 0,
  max: Number.MAX_SAFE_INTEGER
}

global.movieSchema =
  global.movieSchema ||
  new Mongoose.Schema({
    title: {
      type: String,
      required: true,
      trim: true,
      minlength: 1,
      maxLength: 255
    },
    genre: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: 'Genre',
      required: true
    },
    numberInStock: { ...numberSchema },
    dailyRentalRate: { ...numberSchema }
  })

global.movieModel =
  global.movieModel || new Mongoose.model('Movie', global.movieSchema)

exports.Movie = global.movieModel
exports.movieSchema = global.movieSchema
exports.yupSchema = schema
exports.hasErrors = hasErrors
exports.getErrors = getErrors
