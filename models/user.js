const Mongoose = require('mongoose')
const yup = require('yup')
const schemaValidator = require('../helpers/schemaValidator')
const { name, email, password } = require('../helpers/validationSchema')

const schema = yup.object().shape({
  name: name(),
  email: email(),
  password: password()
})

const hasErrors = object => schemaValidator.hasErrors(schema, object)
const getErrors = object => schemaValidator.getErrors(schema, object)

const requiredString = { type: String, required: true, maxLength: 255 }

global.userSchema =
  global.userSchema ||
  new Mongoose.Schema({
    name: { ...requiredString, minlength: 2 },
    email: { ...requiredString, minlength: 8, unique: true },
    password: { ...requiredString, minlength: 8, maxLength: 16 },
    verified: { type: Boolean, default: false },
    verificationKey: { type: String, required: true },
    isAdmin: { type: Boolean, default: false }
  })

global.userModel = global.userModel || Mongoose.model('User', global.userSchema)

exports.User = global.userModel
exports.userSchema = global.userSchema
exports.yupSchema = schema
exports.hasErrors = hasErrors
exports.getErrors = getErrors
