const Mongoose = require('mongoose')
const yup = require('yup')
const { bool, name, phone } = require('../helpers/validationSchema')
const schemaValidator = require('../helpers/schemaValidator')

const schema = yup.object().shape({
  name: name(),
  isGold: bool(),
  phone: phone()
})

const hasErrors = object => schemaValidator.hasErrors(schema, object)
const getErrors = object => schemaValidator.getErrors(schema, object)

global.customerSchema =
  global.customerSchema ||
  new Mongoose.Schema({
    name: { type: String, required: true, minlength: 2, maxLength: 255 },
    isGold: { type: Boolean, default: false },
    phone: { type: Number, require: true }
  })

global.customerModel =
  global.customerModel || new Mongoose.model('Customer', global.customerSchema)

exports.Customer = global.customerModel
exports.customerSchema = global.customerSchema
exports.yupSchema = schema
exports.hasErrors = hasErrors
exports.getErrors = getErrors
