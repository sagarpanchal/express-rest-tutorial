const Mongoose = require('mongoose')
const yup = require('yup')
const { objectId } = require('../helpers/validationSchema')
const schemaValidator = require('../helpers/schemaValidator')

const schema = yup.object().shape({
  customerId: objectId('Customer Id'),
  movieId: objectId('Movie Id')
})

const hasErrors = object => schemaValidator.hasErrors(schema, object)
const getErrors = object => schemaValidator.getErrors(schema, object)

const numberSchema = {
  type: Number,
  required: true,
  min: 0,
  max: Number.MAX_SAFE_INTEGER
}

global.rentalSchema =
  global.rentalSchema ||
  new Mongoose.Schema({
    customer: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: 'Customer',
      required: true
    },
    movie: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: 'Movie',
      required: true
    },
    dailyRentalRate: { ...numberSchema },
    dateOut: { type: Date, required: true, default: Date.now },
    dateReturned: { type: Date },
    rentalFee: { type: Number, min: 0 }
  })

global.rentalModel =
  global.rentalModel || new Mongoose.model('Rental', global.rentalSchema)

exports.Rental = global.rentalModel
exports.rentalSchema = global.rentalSchema
exports.yupSchema = schema
exports.hasErrors = hasErrors
exports.getErrors = getErrors
