const Mongoose = require('mongoose')
const yup = require('yup')
const { name } = require('../helpers/validationSchema')
const schemaValidator = require('../helpers/schemaValidator')

const schema = yup.object().shape({ name: name() })

const hasErrors = object => schemaValidator.hasErrors(schema, object)
const getErrors = object => schemaValidator.getErrors(schema, object)

global.genreSchema =
  global.genreSchema ||
  Mongoose.Schema({
    name: { type: String, required: true, minlength: 5, maxLength: 50 }
  })

global.genreModel =
  global.genreModel || Mongoose.model('Genre', global.genreSchema)

exports.Genre = global.genreModel
exports.genreSchema = global.genreSchema
exports.yupSchema = schema
exports.hasErrors = hasErrors
exports.getErrors = getErrors
