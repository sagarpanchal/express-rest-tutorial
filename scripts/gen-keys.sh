#!/bin/bash

# Generate RSA256 key in PEM format
ssh-keygen -t rsa -b 2048 -m PEM -f jwt_rsa.key

# Convert RSA256 public key to PEM format
openssl rsa -in jwt_rsa.key -pubout -outform PEM -out jwt_rsa.key.pub
