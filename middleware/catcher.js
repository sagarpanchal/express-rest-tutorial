const logger = require('../helpers/logger.js')
const { wentWrong } = require('../helpers/errorMessages')

module.exports = (err, req, res, next) => {
  if (err) {
    logger.error('', err)
    return wentWrong(res)
  }
  next()
}
