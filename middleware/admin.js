const config = require('config')
const debug = require('debug')('app:middleware:admin')
const { sendError } = require('../helpers/errorMessages')

const routes = config.get('adminRoutes')
const routePaths = Object.keys(routes)

module.exports = (req, res, next) => {
  const path = [...req.path.split('/').slice(0, 3), ''].join('/')

  if (routePaths.includes(path) && routes[path].includes(req.method)) {
    if (!req.user.isAdmin) {
      debug('admin route')
      return sendError(res, 'Access Denied', 403)
    }
  }

  next()
}
