const { wentWrong } = require('../helpers/errorMessages')

/**
 * Adds exception handling to unhandled promises
 * @param {Function} handler
 */
exports.asyncMiddleware = handler => {
  return async (req, res, next) => {
    try {
      handler(req, res, next)
    } catch (error) {
      return wentWrong(res)
    }
  }
}
