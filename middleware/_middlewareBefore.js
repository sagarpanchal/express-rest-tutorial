const helmet = require('helmet')
var cors = require('cors')
const compression = require('compression')
const morgan = require('morgan')
const auth = require('./auth')
const admin = require('./admin')
// const logger = require('../helpers/logger.js')
const { sendError } = require('../helpers/errorMessages')

module.exports = (app, express) => {
  app.use(helmet()).use(compression())

  if (app.get('env') === 'development') {
    app.use(cors())
    const log = m => require('debug')('app:request')(m.trim())
    // const toFile = m => logger.log('info', m.trim())
    app.use(morgan('short', { immediate: true, stream: { write: log } }))
    app.use(morgan('short', { stream: { write: log } }))
  }

  app
    .use(express.json(), (err, req, res, next) => {
      return err instanceof SyntaxError
        ? sendError(res, 'Invalid request body.')
        : next()
    })
    .use(auth)
    .use(admin)
}
