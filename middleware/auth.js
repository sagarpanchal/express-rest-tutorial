const config = require('config')
const debug = require('debug')('app:middleware:auth')
const errors = require('../helpers/errorMessages')
const { verifyJWT } = require('../helpers/jwt')
const { User } = require('../models/user')

const { sendError, wentWrong, withoutToken } = errors
const routes = config.get('protectedRoutes')
const routePaths = Object.keys(routes)

module.exports = async (req, res, next) => {
  const path = [...req.path.split('/').slice(0, 3), ''].join('/')
  const methods = [...routes[path], 'OPTIONS']

  if (routePaths.includes(path) && !methods.includes(req.method)) {
    debug(req.method + ' ' + path)

    const token = req.header('x-auth-token')
    if (!token) return withoutToken(res)

    const data = verifyJWT(token)
    debug(data)
    if (!data) return sendError(res, 'Invalid token.')

    const user = await User.findById(data._id)
    if (!user) return wentWrong(res)
    // if (user.verified !== '1') return sendError(res, 'Access denied without email verification ')
    req.user = user
  }

  next()
}
