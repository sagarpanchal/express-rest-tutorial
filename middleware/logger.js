const debug = require('debug')('app:middleware:logger')

const log = (req, res, next) => {
  debug('logging...')
  next()
}

module.exports = log
