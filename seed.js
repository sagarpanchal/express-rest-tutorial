const { Genre } = require('./models/genre')
const { Movie } = require('./models/movie')
const mongoose = require('mongoose')
const config = require('config')

const data = [
  {
    name: 'Comedy',
    movies: [
      { title: 'Airplane', numberInStock: 5, dailyRentalRate: 2 },
      { title: 'The Hangover', numberInStock: 10, dailyRentalRate: 2 },
      { title: 'Wedding Crashers', numberInStock: 15, dailyRentalRate: 2 }
    ]
  },
  {
    name: 'Action',
    movies: [
      { title: 'Die Hard', numberInStock: 5, dailyRentalRate: 2 },
      { title: 'Terminator', numberInStock: 10, dailyRentalRate: 2 },
      { title: 'The Avengers', numberInStock: 15, dailyRentalRate: 2 }
    ]
  },
  {
    name: 'Romance',
    movies: [
      { title: 'The Notebook', numberInStock: 5, dailyRentalRate: 2 },
      { title: 'When Harry Met Sally', numberInStock: 10, dailyRentalRate: 2 },
      { title: 'Pretty Woman', numberInStock: 15, dailyRentalRate: 2 }
    ]
  },
  {
    name: 'Thriller',
    movies: [
      { title: 'The Sixth Sense', numberInStock: 5, dailyRentalRate: 2 },
      { title: 'Gone Girl', numberInStock: 10, dailyRentalRate: 2 },
      { title: 'The Others', numberInStock: 15, dailyRentalRate: 2 }
    ]
  }
]

async function seed() {
  await mongoose.connect(config.get('db'), { useNewUrlParser: true })

  await Movie.deleteMany({})
  await Genre.deleteMany({})

  for (let group of data) {
    const genre = await new Genre({ name: group.name }).save()
    const movies = group.movies.map(movie => ({ ...movie, genre: genre._id }))
    await Movie.insertMany(movies)
  }

  mongoose.disconnect()

  process.stdout.write('\x1b[2JDone!')
}

seed()
